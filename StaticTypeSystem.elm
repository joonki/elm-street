module Main exposing (..)

import Html
import Date
import String


-- Lists like Arrays in other languages


firstNames : List String
firstNames =
    [ "James", "George" ]



-- elements should be of same type
-- Tuples are ordered list of values


pie : ( String, Float )
pie =
    ( "pie", 3.14 )



-- Records


person : { name : String, age : Int }
person =
    { name = "James", age = 42 }



-- update a record: copy updated field into a new record


olderPerson : { name : String, age : Int }
olderPerson =
    { person | age = 43 }



-- Union Types


type Message
    = AddPlayer String
    | Score Int Int



-- message = AddPlayer "James"


message =
    Score 134 2



-- type Messages = Msg1 Int | Msg2 String
-- msg = Msg2 "James"
-- case msg of
--   Msg1 num ->
--     -- do something
--   Msg2 name ->
--     -- do something with "James"
-- Maybe: First element of an empty List returns
-- Nothing : Maybe.Maybe a


emptyList =
    List.head []



-- Result.Result a


someDate =
    Date.fromString "01/01/2017"



-- Ok <Sun Jan 01 2017 00:00:00 GMT+0100 (CET)> : Result.Result String Date.Date


checkDate =
    case someDate of
        Ok val ->
            "It was a legit date"

        Err err ->
            err


action =
    case message of
        AddPlayer name ->
            "Add player " ++ name

        Score id points ->
            "Player id " ++ (toString id) ++ " scored " ++ (toString points)


maybeNum =
    Just 42


add2 a =
    a + 2


result =
    Maybe.map add2 maybeNum



-- Type Annotation
-- wordCount is a new function that combines the functions String.words and List.length


wordCount =
    String.words >> List.length


numberOfWords =
    wordCount "Elm's Static Type System is Nice!"


main =
    -- Html.text(Tuple.first pie)
    -- Html.text(toString action)
    -- Html.text(toString checkDate)
    -- Html.text(toString result)
    Html.text (toString numberOfWords)
