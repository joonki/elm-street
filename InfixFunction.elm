module Main exposing (..)
import Html

-- create own Infix Function (~+) "almost add"
-- that takes two parameters labeled a and b
-- and add 0.1 two their sum

(~+) a b =
  a + b + 0.1

result =
  1 ~+ 2

main =
  Html.text(toString result)

